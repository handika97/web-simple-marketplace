import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import login from "./login";
import register from "./register";
import AppLayout from "../../component/layout/AppLayout";

const User = ({ match }) => {
  return (
    <Switch>
      <Route path={`${match.url}/register`} component={register} />
      <Route path={`${match.url}/login`} component={login} />
      <Redirect exact to={`${match.url}/login`} />
    </Switch>
  );
};

export default User;
