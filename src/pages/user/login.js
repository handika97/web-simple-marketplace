import React, { useState, Fragment, useEffect } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import styled from "styled-components";
import { FaCarSide } from "react-icons/fa";

const LoginPage = ({ match }) => {
  const history = useHistory();
  const [SeePassword, setSeePassword] = useState(false);
  const [windowWidth, setwindowWidth] = useState(window.innerWidth);
  const [windowHeight, setwindowHeight] = useState(window.innerHeight);
  const [mode, setMode] = useState(true);

  useEffect(() => {
    window.addEventListener("resize", () => {
      setwindowHeight(window.innerHeight);
      setwindowWidth(window.innerWidth);
    });
  }, []);
  return (
    <Fragment>
      <Wrapper windowHeight={windowHeight} mode={mode}>
        <div className="baris1">
          <div className="pembatas-horizontal m-t-10"></div>
          <div className="on-off m-t-10 m-l-10" onClick={() => setMode(!mode)}>
            <div className="saklar" />
          </div>
        </div>
        <div className="baris1">
          <div className="pembatas"></div>
          <FaCarSide color="red" size={150} />
          <div className="pembatas"></div>
          <FaCarSide color="green" size={150} />
          <div className="pembatas"></div>
          <FaCarSide color="red" size={150} />
          <div className="pembatas"></div>
          <FaCarSide color="green" size={150} />
          <div className="pembatas"></div>
          <FaCarSide color="green" size={150} />
          <div className="pembatas"></div>
          <FaCarSide color="red" size={150} />
          <div className="pembatas"></div>
        </div>
        <div className="pintu">
          <div className="palang"></div>
        </div>
        <div className="baris2">
          <div className="pembatas"></div>
          <FaCarSide color="red" size={150} />
          <div className="pembatas"></div>
          <FaCarSide color="green" size={150} />
          <div className="pembatas"></div>
          <FaCarSide color="red" size={150} />
          <div className="pembatas"></div>
          <FaCarSide color="green" size={150} />
          <div className="pembatas"></div>
          <FaCarSide color="red" size={150} />
          <div className="pembatas"></div>
          <FaCarSide color="green" size={150} />
          <div className="pembatas"></div>
        </div>
        <div className="pembatas-horizontal"></div>
        <p>Kosong:3</p>
        <p>terisi:3</p>
      </Wrapper>
    </Fragment>
  );
};

export default LoginPage;

const Wrapper = styled.div`
  background-color: black;
  height: 100vh;
  width: 100vw;
  overflow: hidden;
  .pembatas {
    height: 150px;
    width: 20px;
    margin: 0px 10px 0px 10px;
    background-color: white;
  }
  .pembatas-horizontal {
    height: 20px;
    width: 1170px;
    background-color: white;
  }
  .baris1 {
    display: flex;
    flex-direction: row;
  }
  .baris2 {
    /* margin-top: ${({ windowHeight }) => windowHeight / 4}px; */
    display: flex;
    flex-direction: row;
  }
  .marka {
    display: flex;
    flex-direction: row;
  }
  p {
    font-size: 30px;
    color: white;
  }
  .m-t-10 {
    margin: 10px 0px 0px 0px;
  }
  .palang {
    height: 150px;
    width: 20px;
    margin: 0px 150px 0px 150px;
    background-color: green;
    align-self: flex-end;
    display: flex;
  }
  .pintu {
    width: 100%;
    display: flex;
    justify-content: flex-end;
  }
  .on-off {
    width: 70px;
    height: 30px;
    background-color: white;
    border-radius: 100px;
    padding: 2px;
    display: flex;
    justify-content: flex-${({ mode }) => (mode ? "start" : "end")};
    transition: 0.5s;
    .saklar {
      transition: 0.5s;
      width: 30px;
      height: 25px;
      background-color: black;
      border-radius: 100px;
    }
  }
  .m-l-10 {
    margin-left: 10px;
  }
`;
