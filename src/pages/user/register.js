import React, { useState, Fragment, useEffect } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import styled from "styled-components";
import { useSelector, useDispatch } from "react-redux";
import Axios from "axios";
import { Register } from "../../redux/features/authSlice";
import Input from "../../component/cell/Input";
import Button from "../../component/cell/Button";
import Message from "../../component/cell/Message";
import Modal from "../../component/atom/Modal";

const LoginPage = ({ match }) => {
  const history = useHistory();
  const [SeePassword, setSeePassword] = useState(false);
  const [Password, setPassword] = useState("");
  const [PasswordStatus, setPasswordStatus] = useState(true);
  const [Email, setEmail] = useState("");
  const [Name, setName] = useState("");
  const [Phone, setPhone] = useState("");
  const [EmailStatus, setEmailStatus] = useState(true);
  const dispatch = useDispatch();
  const [message, setMessage] = useState(false);
  const [status, setStatus] = useState("");
  const [modal, setModal] = useState(false);
  const [Loading, setLoading] = useState(false);
  useEffect(() => {
    if (Email.length > 0) {
      setTimeout(() => {
        validateEmail();
      }, 1000);
    }
  }, [Email]);

  function validateEmail() {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return setEmailStatus(re.test(String(Email).toLowerCase()));
  }

  const LoginActions = (e) => {
    e.preventDefault();
    if (!Email) {
      setEmailStatus(false);
    } else if (!Password) {
      setPasswordStatus(false);
    } else if (
      EmailStatus &&
      PasswordStatus &&
      Password &&
      Email &&
      Name &&
      Phone
    ) {
      setLoading(true);
      setModal(true);
      dispatch(
        Register(history, Email, Password, Name, Phone, () => {
          setStatus("Error");
          setMessage(true);
          setLoading(false);
          setModal(false);
          setTimeout(() => setMessage(false), 3000);
        })
      );
    }
  };
  useEffect(() => {
    console.log(Email, Password);
  }, [Email, Password]);
  return (
    <Fragment>
      <Wrapper>
        <div className="container">
          <div className="login-box">
            <div className="login">
              <p className="title">Registrasi</p>
              <div className="w-100">
                <form onSubmit={LoginActions}>
                  <Input
                    full
                    className={"input"}
                    placeholder="Ketik Nama Anda"
                    type={"text"}
                    Value={Name}
                    onChange={(e) => setName(e)}
                    Title="Nama"
                  />
                  <Input
                    full
                    className={EmailStatus ? "input" : "wrong-input"}
                    placeholder="Ketik Email Anda"
                    type={"email"}
                    Title="Email"
                    Value={Email}
                    onChange={(e) => setEmail(e)}
                    Correction={EmailStatus}
                    correctionText={
                      Email ? "Format Email Salah" : "Masukan Email"
                    }
                  />
                  {/* </div> */}
                  {/* <div className="w-100"> */}
                  <Input
                    full
                    password
                    className={
                      (PasswordStatus ? "input" : "wrong-input") + " password"
                    }
                    placeholder="Ketik Kata Sandi Anda"
                    type={!SeePassword ? "password" : "text"}
                    Value={Password}
                    Title="Password"
                    onChange={(e) => setPassword(e)}
                    SeePassword={SeePassword}
                    onClick={() => setSeePassword(!SeePassword)}
                    Correction={PasswordStatus}
                    correctionText={
                      Password.length
                        ? "Password Belum Valid"
                        : "Masukan Password"
                    }
                  />
                  <Input
                    full
                    className={"input"}
                    placeholder="Ketik Nomer Hp Anda"
                    type={"text"}
                    Value={Phone}
                    onChange={(e) => setPhone(e)}
                    Title="Phone"
                  />
                  <Button className="orange" Title="Daftar" />
                </form>
                <p
                  className="title"
                  onClick={() => history.replace("/user/login")}
                >
                  Login Sekarang
                </p>
              </div>
            </div>
          </div>
        </div>
      </Wrapper>
      <Modal
        showModal={modal}
        toggleModal={() => setModal(false)}
        loading={Loading}
      />
      <Message ShowMessage={message} Status={status} />
    </Fragment>
  );
};

export default LoginPage;

const Wrapper = styled.div`
  .container {
    background-color: var(--primary-color);
    width: 100vw;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    .login-box {
      width: 450px;
      background-color: white;
      border-radius: 8px;
      box-shadow: 0px 2px 8px 0px rgba(0, 0, 0, 0.1);
      display: flex;
      justify-content: center;
      align-items: center;
      padding: 10px;
      .login {
        border: 5px white solid;
        width: 100%;
        height: 100%;
        background-color: white;
        border-radius: 8px;
        display: flex;
        justify-content: center;
        flex-direction: column;
        align-items: center;
        padding: 32px;
        .title {
          color: #868e96;
          font-weight: bold;
          margin-bottom: 36px;
        }
        .w-100 {
          width: 100%;
        }
      }
    }
  }
`;
