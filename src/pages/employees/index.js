import React from "react";
import {
  Route,
  Switch,
  Redirect,
  useRouteMatch,
  useHistory,
} from "react-router-dom";
import Product from "./Product";
import Penjualan from "./Penjualan";
import Pembelian from "./Pembelian";
import MyProduct from "./MyProduct";
import AppLayout from "../../component/layout/AppLayout";

const User = ({ match }) => {
  return (
    <AppLayout>
      <Switch>
        <Route path={`${match.url}/product`} component={Product} />
        <Route path={`${match.url}/penjualan`} component={Penjualan} />
        <Route path={`${match.url}/myproduct`} component={MyProduct} />
        <Route path={`${match.url}/pembelian`} component={Pembelian} />
        <Redirect exact to={`${match.url}/product`} />
      </Switch>
    </AppLayout>
  );
};

export default User;
