import React, { useState, Fragment, useEffect, useMemo } from "react";
import { useSelector, useDispatch } from "react-redux";
import { MdAdd } from "react-icons/md";
import styled from "styled-components";
import { get } from "../../redux/features/slice";
import BodyPage from "../../component/cell/BodyPage";
import ContainerBox from "../../component/cell/ContainerBox";
import Container from "../../component/cell/Container";
import Table from "../../component/cell/Table";
import Modal from "../../component/atom/Modal";
import Button from "../../component/cell/Button";
import Input from "../../component/cell/Input";
import Message from "../../component/cell/Message";

const Login = ({ match }) => {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const [modal, setModal] = useState(true);
  const [message, setMessage] = useState(false);
  const [status, setStatus] = useState("");
  const [Loading, setLoading] = useState(true);
  const [Data, setData] = useState([]);
  useEffect(() => {
    dispatch(
      get(
        `/trx/seller/${auth.id}`,
        (res) => {
          setData(res.data.data);
          setStatus("Succeess");
          setModal(false);
        },
        (err) => {
          setStatus("Error");
          setModal(false);
        },
        () => {
          setLoading(false);
          setMessage(true);
          setTimeout(() => setMessage(false), 3000);
        }
      )
    );
  }, []);

  const columns = React.useMemo(
    () => [
      {
        Header: "Nomer Penjualan",
        accessor: "id",
      },
      {
        Header: "Nama Product",
        accessor: "name",
      },
      {
        Header: "Harga",
        accessor: "price",
      },
      {
        Header: "Jumlah",
        accessor: "qty",
      },
      {
        Header: "Total",
        accessor: "total_price",
      },
    ],
    []
  );

  return (
    <Fragment>
      <BodyPage>
        <Container>
          <ContainerBox
            style={{
              marginBottom: 36,
            }}
          >
            <Table columns={columns} data={Data} />
          </ContainerBox>
        </Container>
      </BodyPage>
      <Modal showModal={modal} loading={Loading}></Modal>
      <Message ShowMessage={message} Status={status} />
    </Fragment>
  );
};

export default Login;

const Filter = styled.div`
  display: flex;
  .w-100 {
    width: 100px;
  }
`;
