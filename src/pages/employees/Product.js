import React, { useState, Fragment, useEffect, useMemo } from "react";
import { useSelector, useDispatch } from "react-redux";
import styled from "styled-components";
import { post, get } from "../../redux/features/slice";
import BodyPage from "../../component/cell/BodyPage";
import ContainerBox from "../../component/cell/ContainerBox";
import Container from "../../component/cell/Container";
import DropDown from "../../component/cell/DropDown";
import Modal from "../../component/atom/Modal";
import Input from "../../component/cell/Input";
import Message from "../../component/cell/Message";

const Login = ({ match }) => {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const [modal, setModal] = useState(true);
  const [Loading, setLoading] = useState(true);
  const [Data, setData] = useState([]);
  const [Category, setCategory] = useState("");
  const [IdCategory, setIdCategory] = useState("all");
  const [Product, setProduct] = useState({});
  const [qty, setqty] = useState("");
  const [metode_pay, setmetode_pay] = useState("");
  const [message, setMessage] = useState(false);
  const [status, setStatus] = useState("");

  useEffect(() => {
    // setDate(moment(new Date()).format("YYYY-MM-DD"));
    setLoading(true);
    setModal(true);
    dispatch(
      get(
        `/product/${IdCategory}/${auth.id}`,
        (res) => {
          console.log(res.data.data);
          setData(res.data.data);
          setStatus("Succeess");
        },
        (err) => {
          setStatus("Error");
        },
        () => {
          setLoading(false);
          setModal(false);
          setMessage(true);
          setTimeout(() => setMessage(false), 3000);
        }
      )
    );
    dispatch(
      get(
        `/category`,
        (res) => {
          console.log(res.data.data);
          setCategory(res.data.data);
        },
        (err) => {},
        () => {
          setLoading(false);
          setModal(false);
          setMessage(true);
          setTimeout(() => setMessage(false), 3000);
        }
      )
    );
  }, [IdCategory]);

  useEffect(() => {
    if (!modal) {
      setProduct({});
      setqty("");
    }
  }, [modal]);

  return (
    <Fragment>
      <BodyPage>
        <Container>
          <ContainerBox
            style={{
              marginBottom: 36,
            }}
          >
            {Category && (
              <DropDown
                noPadding
                Action
                data={[{ id: "all", name: "all" }, ...Category]}
                value={IdCategory}
                onChange={(e) => {
                  setIdCategory(e);
                }}
              />
            )}
            <Wrapper>
              {Data.map((item) => (
                <div
                  className="Container-Display"
                  onClick={() => {
                    setProduct(item);
                    setModal(true);
                  }}
                >
                  <div className="product-Container">
                    <div className="photo-product"></div>
                    <div className="desc-product">
                      <div className="desc-Container">
                        <p>{item.product_name}</p>
                        <p>Rp. {item.price}</p>
                        <p>Penjual= {item.seller_name}</p>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </Wrapper>
          </ContainerBox>
        </Container>
      </BodyPage>
      <Modal
        showModal={modal}
        toggleModal={() => setModal(false)}
        loading={Loading}
        okeLabel={"Beli"}
        onClick={(e) => {
          setLoading(true);
          dispatch(
            post(
              `/trx/buy`,
              {
                seller: Product.seller_id,
                buyyer: auth.id,
                metode_pay: metode_pay,
                item: Product.id_product,
                qty: qty,
                total_price: Product.price * qty,
              },
              (res) => {
                setStatus("Succeess");
              },
              (err) => {},
              () => {
                setLoading(false);
                setModal(false);
                setMessage(true);
                setTimeout(() => setMessage(false), 3000);
              }
            )
          );
        }}
        toggleModal={() => setModal(false)}
        headerLabel={"Beli Product"}
      >
        <div>
          {Product && (
            <>
              <p>{Product.product_name}</p>
              <p>Rp. {Product.price}</p>
            </>
          )}
          <Input
            type={"number"}
            full
            className={"input-modal"}
            placeholder="Jumlah"
            Value={qty}
            onChange={(e) => setqty(e)}
          />
          <p>Total= Rp. {Product.price * qty}</p>
          <DropDown
            noPadding
            Action
            data={[
              { id: "all", name: "all" },
              { id: "bca", name: "BCA" },
              { id: "bri", name: "BRI" },
              { id: "bni", name: "BNI" },
              { id: "mandiri", name: "Mandiri" },
            ]}
            value={metode_pay}
            onChange={(e) => {
              setmetode_pay(e);
            }}
          />
        </div>
      </Modal>
      <Message ShowMessage={message} Status={status} />
    </Fragment>
  );
};

export default Login;

const Wrapper = styled.div`
  display: flex;
  /* justify-content: space-between; */
  align-items: center;
  flex-direction: row;
  /* width: 70%; */
  flex-wrap: wrap;
  .Container-Display {
    display: flex;
    flex-wrap: wrap;
    flex-direction: row;
  }
  .product-Container {
    display: flex;
    width: 25%;
    min-width: 200px;
    height: 250px;
    padding: 20px;
    flex-direction: column;
    min-width: 250px;
  }
  .photo-product {
    background-color: black;
    width: 100%;
    height: 175px;
    box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.5);
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
  }
  .desc-product {
    background-color: white;
    width: 100%;
    height: 75px;
    display: flex;
    flex-direction: row;
    padding: 10px;
    justify-content: space-between;
    border-bottom-left-radius: 5px;
    border-bottom-right-radius: 5px;
    box-shadow: 0px 0px 8px rgba(0, 0, 0, 1);
  }
  .desc-Container {
    display: flex;
    flex-direction: column;
  }
  .add-channel {
    display: flex;
    align-items: center;
    justify-content: center;
  }
  .loading-Container {
    display: flex;
    width: 25%;
    height: 170px;
    padding: 20px;
    flex-direction: column;
    min-width: 250px;
  }
`;
