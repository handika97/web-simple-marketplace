import React, { useState, Fragment, useEffect, useMemo } from "react";
import { useSelector, useDispatch } from "react-redux";
import { MdAdd } from "react-icons/md";
import styled from "styled-components";
import { post, get } from "../../redux/features/slice";
import BodyPage from "../../component/cell/BodyPage";
import ContainerBox from "../../component/cell/ContainerBox";
import Container from "../../component/cell/Container";
import Table from "../../component/cell/Table";
import Modal from "../../component/atom/Modal";
import Button from "../../component/cell/Button";
import Input from "../../component/cell/Input";
import Message from "../../component/cell/Message";
import DropDown from "../../component/cell/DropDown";
import { getDefaultNormalizer } from "@testing-library/react";

const Login = ({ match }) => {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const [modal, setModal] = useState(true);
  const [message, setMessage] = useState(false);
  const [status, setStatus] = useState("");
  const [Category, setCategory] = useState("");
  const [Loading, setLoading] = useState(true);
  const [attribute, setAttribute] = useState({
    name: "",
    price: "",
    category: "",
  });
  const [Data, setData] = useState([]);
  useEffect(() => {
    getData();
    dispatch(
      get(
        `/category`,
        (res) => {
          console.log(res.data.data);
          setCategory(res.data.data);
        },
        (err) => {},
        () => {
          setLoading(false);
          setModal(false);
          setMessage(true);
          setTimeout(() => setMessage(false), 3000);
        }
      )
    );
  }, []);

  const getData = () => {
    dispatch(
      get(
        `/product/may/${auth.id}`,
        (res) => {
          console.log(res.data.data, "data");
          setData(res.data.data);
          setStatus("Succeess");
          setModal(false);
        },
        (err) => {
          setStatus("Error");
          setModal(false);
        },
        () => {
          setLoading(false);
          setMessage(true);
          setTimeout(() => setMessage(false), 3000);
        }
      )
    );
  };

  const columns = React.useMemo(
    () => [
      {
        Header: "Id",
        accessor: "id",
      },
      {
        Header: "Nama Product",
        accessor: "name",
      },
      {
        Header: "Harga",
        accessor: "price",
      },
      {
        Header: "Kategori",
        accessor: "category_name",
      },
    ],
    []
  );

  return (
    <Fragment>
      <BodyPage>
        <Container>
          <ContainerBox
            style={{
              marginBottom: 36,
            }}
          >
            <Filter>
              <div className="w-100">
                <Button
                  className="add"
                  onClick={() => {
                    setModal(true);
                  }}
                  Title={
                    <div
                      style={{
                        justifyContent: "center",
                        alignItems: "center",
                        display: "flex",
                      }}
                    >
                      <MdAdd size={20} />
                      ADD
                    </div>
                  }
                />
              </div>
            </Filter>
            <Table columns={columns} data={Data} />
          </ContainerBox>
        </Container>
      </BodyPage>
      <Modal
        okeLabel={"Tambah"}
        showModal={modal}
        loading={Loading}
        onClick={(e) => {
          setLoading(true);
          dispatch(
            post(
              `/product/create`,
              { id_user: auth.id, ...attribute },
              (res) => {
                setStatus("Succeess");
                getData();
              },
              (err) => {},
              () => {
                setLoading(false);
                setModal(false);
                setMessage(true);
                setTimeout(() => setMessage(false), 3000);
              }
            )
          );
        }}
        toggleModal={() => setModal(false)}
        headerLabel={"Tambah Product"}
      >
        <div className="w-100">
          <Input
            full
            className={"input-modal"}
            placeholder="Nama Product"
            type={"text"}
            Value={attribute.name}
            onChange={(e) => setAttribute({ ...attribute, name: e })}
          />
          <Input
            full
            className={"input-modal"}
            placeholder="Harga"
            type={"number"}
            Value={attribute.price}
            onChange={(e) => setAttribute({ ...attribute, price: e })}
          />
          {Category && (
            <DropDown
              noPadding
              Action
              data={[{ id: "Category", name: "Category" }, ...Category]}
              value={attribute.category}
              onChange={(e) => setAttribute({ ...attribute, category: e })}
            />
          )}
        </div>
      </Modal>
      <Message ShowMessage={message} Status={status} />
    </Fragment>
  );
};

export default Login;

const Filter = styled.div`
  display: flex;
  .w-100 {
    width: 100px;
  }
`;
