import React, { useState, Fragment, useEffect } from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import { useDispatch } from "react-redux";
import { BiUserCircle } from "react-icons/bi";
import { layout } from "../../settings";
import { Logout } from "../../redux/features/authSlice";

const AppLayout = (props) => {
  let history = useHistory();
  let dispatch = useDispatch();
  const [windowWidth, setwindowWidth] = useState(window.innerWidth);
  const [windowHeight, setwindowHeight] = useState(window.innerHeight);

  useEffect(() => {
    window.addEventListener("resize", () => {
      setwindowHeight(window.innerHeight);
      setwindowWidth(window.innerWidth);
    });
  }, []);

  return (
    <Fragment>
      <Wrapper windowWidth={windowWidth} windowHeight={windowHeight}>
        <div className="header">
          <div className="Header-Menu">
            <p onClick={() => history.replace("/app/product")}>Beranda</p>
            <p onClick={() => history.replace("/app/pembelian")}>
              Riwayat Pembelian
            </p>
            <p onClick={() => history.replace("/app/penjualan")}>
              Riwayat Penjualan
            </p>
            <p onClick={() => history.replace("/app/myproduct")}>Produk Ku</p>
          </div>
          <div className="buttonProfile">
            <BiUserCircle size={30} color="black" />
            <div className="logout" onClick={() => dispatch(Logout())}>
              <p>Logout</p>
            </div>
          </div>
        </div>
        <div className="container-app">{props.children}</div>
      </Wrapper>
    </Fragment>
  );
};

export default AppLayout;

const Wrapper = styled.div`
  .header {
    width: ${(props) => props.windowWidth}px;
    position: fixed;
    height: ${layout.navbarHeight}px;
    background-color: white;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0 20px;
    box-sizing: border-box;
    transition: 0.5s;
    .Header-Menu {
      display: flex;
      flex-direction: row;
      gap: 10px;
      p {
        cursor: pointer;
        :hover {
          color: red;
        }
      }
    }
    .buttonProfile {
      .logout {
        position: absolute;
        background-color: white;
        padding: 10px;
        box-sizing: border-box;
        right: 10px;
        border: 0.5px grey solid;
        border-radius: 5px;
        visibility: hidden;
      }
      :hover .logout {
        visibility: visible;
        cursor: pointer;
      }
      .logout:hover {
        background-color: #64b0f2;
        color: white;
      }
    }
  }
  .container-app {
    padding: ${layout.navbarHeight + 20}px 0px 0px 0px;
    transition: 0.5s;
    height: 100vh;
    box-sizing: border-box;
    display: flex;
    background-color: #d7d7d7;
  }
`;
