import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { layout } from "../../settings";

export default function BodyPage({ children }) {
  const [windowWidth, setwindowWidth] = useState(window.innerWidth);
  const [windowHeight, setwindowHeight] = useState(window.innerHeight);

  useEffect(() => {
    window.addEventListener("resize", () => {
      setwindowHeight(window.innerHeight);
      setwindowWidth(window.innerWidth);
    });
  }, []);

  return (
    <StyledDiv windowHeight={windowHeight} windowWidth={windowWidth}>
      {children}
    </StyledDiv>
  );
}

const StyledDiv = styled.div`
  width: 100%;
  height: ${({ windowHeight }) => windowHeight - layout.navbarHeight}px;
  background-color: var(--background-color);
  overflow: auto;
`;
