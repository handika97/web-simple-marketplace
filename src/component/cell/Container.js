import React from 'react';
import styled from 'styled-components';

export default function ContainerBox({children}) {
  return (
    <StyledDiv>
      {children}
    </StyledDiv>
  )
}

const StyledDiv = styled.div`
  padding: 0 20px;
`;
