import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useTable, usePagination, useRowSelect } from "react-table";
import DropDown from "./DropDown";
import Input from "./Input";
import { staticData } from "../../settings";
import { BsSearch } from "react-icons/bs";
import ReactHTMLTableToExcel from "react-html-table-to-excel";
export default function Table({ columns, data }) {
  // Use the state and functions returned from useTable to build your UI
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
    },
    usePagination
  );

  return (
    <Styles>
      <TableContainer>
        <table {...getTableProps()} id="table">
          <thead>
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <th {...column.getHeaderProps()}>
                    {column.render("Header")}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {rows.map((row, i) => {
              prepareRow(row);
              return (
                <>
                  <tr {...row.getRowProps()}>
                    {row.cells.map((cell) => {
                      return (
                        <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                      );
                    })}
                  </tr>
                </>
              );
            })}
          </tbody>
        </table>
      </TableContainer>
    </Styles>
  );
}

const Styles = styled.div`
  width: 100%;
  .pagination {
    display: flex;
    justify-content: flex-end;
    .buttonPagination {
      border: 1px solid var(--border-input-color);
      border-collapse: collapse;
      border-radius: 2px;
      padding: 10px 12px;
      margin-right: 4px;
      cursor: pointer;
      font-size: 14px;
      color: var(--main-text-color);
      transition: 0.1s;
      p {
        -webkit-user-select: none; /* Chrome 49+ */
        -moz-user-select: none; /* Firefox 43+ */
        -ms-user-select: none; /* No support yet */
        user-select: none;
      }
      &:hover {
        background-color: var(--hover-primary-color);
      }
      &:active {
        background-color: var(--primary-color);
        color: white;
      }
      &.active {
        background-color: var(--primary-color);
        color: white;
        &:hover {
          background-color: var(--primary-color);
        }
      }
      &.dots {
        border: none;
        cursor: default;
        &:hover,
        &:active {
          background-color: unset;
          color: unset;
        }
      }
    }
  }
  .topTable {
    display: flex;
    justify-content: space-between;
    align-items: flex-end;
    padding: 12px 0;
    .showItems {
      display: flex;
      align-items: center;
      gap: 8px;
      p {
        font-size: 14px;
        color: var(--sub-text-color);
      }
    }
  }
`;

const TableContainer = styled.div`
  width: 100%;
  margin-bottom: 24px;
  /* max-height: 445px; */
  overflow-x: auto;
  ::-webkit-scrollbar {
    height: 4px;
  }
  ::-webkit-scrollbar-thumb {
    background-color: var(--border-color);
    border-radius: 20px;
  }
  table {
    border-top: 1px solid var(--border-color);
    width: 100%;
    border-collapse: separate;
    thead {
      font-weight: bold;
      text-align: left;
      th {
        background-color: white;
        position: sticky;
        top: 0;
        padding: 22px;
        border-top: 1px solid white;
      }
    }
    th,
    td {
      padding: 16px 22px;
      border-bottom: 1px solid var(--border-color);
      font-size: 14px;
      color: var(--main-text-color);
      &:first-child {
        width: 5%;
        white-space: nowrap;
      }
    }
    tr:hover {
      background-color: var(--border-color);
    }
  }
`;
