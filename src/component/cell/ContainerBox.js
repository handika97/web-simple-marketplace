import React from 'react';
import styled from 'styled-components';

export default function ContainerBox({children, ...rest}) {
  return (
    <StyledDiv {...rest}>
      {children}
    </StyledDiv>
  )
}

const StyledDiv = styled.div`
  padding: 20px;
  min-width: 100px;
  min-height: 80px;
  background-color: white;
  border-radius: 4px;
`;


