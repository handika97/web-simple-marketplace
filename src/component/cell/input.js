import React, { useState, Fragment, useEffect } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import { BsEyeSlash, BsEye } from "react-icons/bs";

import styled, { css } from "styled-components";

const Input = ({
  onClick = () => {},
  full,
  style,
  className,
  customWidth,
  Title,
  Value,
  onChange = () => {},
  placeholder,
  type,
  id,
  password,
  icon,
  SeePassword,
  Correction,
  correctionText,
}) => {
  return (
    <Wrapper
      customWidth={customWidth}
      icon={icon}
      full={full}
      style={style}
      className={className}
    >
      <div className={className}>
        {icon && <label htmlFor={id}>{icon}</label>}
        <input
          placeholder={placeholder}
          id={id}
          type={type}
          value={Value}
          onChange={(e) => onChange(e.target.value)}
          size="0"
        />
        {password ? (
          SeePassword ? (
            <BsEye
              className="icons-password"
              onClick={() => onClick()}
              size={20}
            />
          ) : (
            <BsEyeSlash
              className="icons-password"
              onClick={() => onClick()}
              size={20}
            />
          )
        ) : null}
      </div>
      {!Correction && (
        <p
          style={{
            color: "red",
            fontSize: 12,
            marginTop: 2,
            alignSelf: "flex-start",
            position: "absolute",
          }}
        >
          {correctionText}
        </p>
      )}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  position: relative;
  margin-bottom: ${(props) =>
    props.className === "input-modal" ||
    props.className === "input-modal password"
      ? 0
      : 24}px;
  /* margin-bottom: 24px; */
  > div {
    position: relative;
    border: 1px var(--border-input-color) solid;
    border-radius: 2px;
    max-width: ${({ customWidth }) =>
      customWidth ? customWidth + "px" : "150px"};
    ${({ full }) =>
      full &&
      css`
        width: 100%;
        max-width: 100%;
      `}
    > label {
      position: absolute;
      top: 50%;
      left: 12px;
      transform: translateY(-50%);
      * {
        color: var(--sub-text-color);
      }
    }
    input {
      border: none;
      outline: none;
      width: 100%;
      height: 100%;
      padding: ${({ icon }) => {
        return icon ? "10px 12px 10px 40px" : "10px 12px";
      }};
      color: grey;
      border-radius: 2px;
      transition: 0.2s;
    }
    input:focus {
      box-shadow: 0 0 0 1px var(--border-primary-color);
    }
  }
  .input-modal {
    border: none;
    outline: none;
    width: 100%;
    height: 100%;
    padding: ${({ icon }) => {
      return icon ? "10px 12px 10px 40px" : "10px 12px";
    }};
    color: grey;
    border-radius: 2px;
    transition: 0.2s;
    input {
      border: 0.3px solid black;
    }
    input:focus {
      border: 0.3px solid var(--border-primary-color);
      /* box-shadow: 0 0 0 1px var(--border-primary-color); */
    }
  }
  .wrong-input {
    input:focus {
      box-shadow: 0 0 0 1px red;
    }
  }
  .password {
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
    box-sizing: border-box;
    input {
      padding-right: 40px;
    }
    .icons-password {
      position: absolute;
      right: 10px;
    }
  }
  input::-ms-reveal,
  input::-ms-clear {
    display: none;
  }
`;

export default Input;

Input.defaulProps = {
  password: false,
};
