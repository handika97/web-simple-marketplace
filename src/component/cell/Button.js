import React, { useState, Fragment, useEffect } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import { useDispatch } from "react-redux";

import styled, { css } from "styled-components";

const Button = ({
  onClick = () => {},
  className,
  Title,
  oke,
  cancel,
  small,
}) => {
  return (
    <Wrapper className={className} oke={oke} cancel={cancel} small={small}>
      <div className={className} onClick={() => onClick()}>
        <p className="button-Title">{Title}</p>
      </div>
    </Wrapper>
  );
};

const Wrapper = styled.button`
  width: 100%;
  padding: 0px 0px;
  background-color: white;
  border: 0px white solid;
  ${(props) =>
    props.className === "google" &&
    css`
      .google {
        width: 100%;
        border: 1px solid #757575;
        background-color: white;
        border-radius: 50px;
        height: 40px;
        margin: 5px 0px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: row;
        .button-Title {
          font-size: 14px;
          font-weight: 200;
          color: grey;
        }
      }
    `}
  ${(props) =>
    props.className === "orange" &&
    css`
      .orange {
        width: 100%;
        border: 1px solid grey;
        background-color: #0283d6;
        border-radius: 5px;
        height: 40px;
        margin: 10px 0px;
        display: flex;
        justify-content: center;
        align-items: center;
        .button-Title {
          font-size: 14px;
          font-weight: 200;
          color: white;
        }
      }
    `}
  ${(props) =>
    props.className === "add" &&
    css`
      .add {
        width: 100%;
        border: 1px solid #4c7a34;
        background-color: #4c7a34;
        border-radius: 5px;
        height: 40px;
        margin: 10px 0px;
        display: flex;
        justify-content: center;
        align-items: center;
        .button-Title {
          font-size: 17px;
          font-weight: 500;
          color: white;
        }
      }
    `}
    ${(props) =>
    props.oke &&
    css`
      border: 1px solid #4c7a34;
      background-color: #4c7a34;
      color: white;
      box-shadow: none;
      padding: 6px 12px;
      font-weight: normal;
      max-width: 80px;
      &:focus {
        outline: none;
        border: 1px solid #4c7a34;
        box-shadow: 0 0 2px 3px rgba(168, 204, 55, 0.3);
      }
    `};
  ${(props) =>
    props.cancel &&
    css`
      border: 1px solid #bababa;
      background-color: #bababa;
      color: white;
      box-shadow: none;
      padding: 6px 12px;
      font-weight: normal;
      max-width: 80px;
      &:focus {
        outline: none;
        border: 1px solid #bababa;
        box-shadow: 0 0 2px 3px rgba(186, 186, 186, 0.3);
      }
    `};
  cursor: pointer;
`;

export default Button;
