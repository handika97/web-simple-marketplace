import { createSlice } from "@reduxjs/toolkit";
import { post, patch } from "./slice";
import { BaseUrl } from "../../utilities/BaseUrl";
export const auth = createSlice({
  name: "auth",
  initialState: {
    isLogin: false,
    id: "",
  },
  reducers: {
    startAsync: (state) => {
      state.loading = true;
    },
    stopAsync: (state) => {
      state.loading = false;
    },
    loginSuccess: (state, action) => {
      state.isLogin = true;
      state.id = action.payload.id;
    },
    logout: (state) => {
      state.isLogin = false;
    },
  },
});

export const { startAsync, stopAsync, loginSuccess, logout } = auth.actions;

export default auth.reducer;

// ---------------- ACTION ---------------

const defaultBody = null;

export const Login = (history, Email, Password, actionsErr = () => {}) => (
  dispatch
) => {
  dispatch(
    post(
      "/kyc/login",
      { email: Email, password: Password },
      (res) => {
        dispatch(loginSuccess(res.data.data));
        history.replace("/app");
      },
      (err) => {
        actionsErr();
      }
    )
  );
};
export const Register = (
  history,
  Email,
  Password,
  Name,
  Phone,
  actionsErr = () => {}
) => (dispatch) => {
  dispatch(
    post(
      "/kyc/register",
      { email: Email, password: Password, name: Name, phone: Phone },
      (res) => {
        history.replace("/user/login");
      },
      (err) => {
        actionsErr();
      }
    )
  );
};
export const Logout = () => (dispatch) => {
  dispatch(logout());
};
